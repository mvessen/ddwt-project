<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$comment = $_POST['comment'];
	$sender = $_SESSION['userid'];
	$receiver = $_POST['profileuser'];

	$query = $db->prepare('INSERT INTO comment (comment, sender, receiver, senddate)
							VALUES (?, ?, ?, CURRENT_TIMESTAMP)');
	$query->execute(array($comment, $sender, $receiver));

	header('Location: ../Users/profile.php?userid='.$receiver.'');
?>