<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$userid = $_SESSION['userid'];
	$commentid = $_POST['commentid'];

	$query = $db->prepare('DELETE FROM comment WHERE commentid=?');
	$query->execute(array($commentid));
	
	header('Location: ../Users/profile.php?userid='.$userid.'');
?>