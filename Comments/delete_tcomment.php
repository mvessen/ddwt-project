<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$userid = $_SESSION['userid'];
	$topicid = $_POST['topicid'];
	$tcommentid = $_POST['tcommentid'];

	$query = $db->prepare('DELETE FROM tcomment WHERE tcommentid=? AND userid=? AND topicid=?');
	$query->execute(array($tcommentid, $userid, $topicid));
	
	header('Location: tcomments_form.php?topicid='.$topicid.'');
?>