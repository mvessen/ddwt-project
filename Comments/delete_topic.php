<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$userid = $_SESSION['userid'];
	$topicid = $_POST['topicid'];

	$query = $db->prepare('DELETE FROM topic WHERE topicid=? AND userid=?');
	$query->execute(array($topicid, $userid));

	$tquery = $db->prepare('DELETE FROM tcomment WHERE topicid=?');
	$tquery->execute(array($topicid));
	
	header('Location: ../Users/index.php');
?>