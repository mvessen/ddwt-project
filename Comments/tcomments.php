<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$tcomment = $_POST['tcomment'];
	$userid = $_SESSION['userid'];
	$topicid = $_POST['topicid'];

	$query = $db->prepare('INSERT INTO tcomment (comment, userid, topicid, postdate)
							VALUES (?, ?, ?, CURRENT_TIMESTAMP)');
	$query->execute(array($tcomment, $userid, $topicid));
	
	header('Location: tcomments_form.php?topicid='.$topicid.'');
?>