<?php session_start(); ?>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>Topic Comments</title>
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	    <link rel="stylesheet" type="text/css" href="../main.css">
	    <script type="text/javascript" src="../toggle.js"></script>
  	</head>
	<body>
		<header>
			<div id="header">
				<!-- Home -->
				<a id="home" href="../Users/index.php"><img src="../forum.png" alt="forum logo"></a>

<?php 
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');
	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	if (isset($_SESSION['userid'])) {
		$myuser = $_SESSION['userid'];			
?>
				<!-- Login/Register//Logout/Profile -->
				<div id="links">
					<a href="../Users/profile.php?userid=<?=$myuser?>">My profile</a>
					<a href="../Users/logout.php">Logout</a>
<?php
	} else {
?>
					<a href="../Users/login_form.php">Login</a>
					<a href="../Users/register_form.php">Register</a>
<?php  
	}
?>
				</div>
			</div>
		</header>
		<section id="topic_container">
			<!-- Title/description/startdate/author topic -->
<?php
	$topicid = $_GET['topicid'];

	$query = $db->prepare('SELECT topic.*, user.username FROM topic, user WHERE topic.topicid=? AND user.userid = topic.userid');
	$query->execute(array($topicid));
	$topic = $query->fetchAll();

	foreach ($topic as $row) {
		$title = htmlspecialchars($row['title']);
		$description = htmlspecialchars($row['description']);
		$startdate = htmlspecialchars($row['startdate']);
		$username = htmlspecialchars($row['username']);
		$userid = htmlspecialchars($row['userid']);
?>

		<div id='topic_info'>
			<h4><?=$title?></h4>
			<div id='topicdes'><?=$description?></div>
			<div class="t_info">Submitted on <?=$startdate?> by <a href="../Users/profile.php?userid=<?=$userid?>"><?=$username?></a></div>
		</div>
<?php
	}
	if (isset($_SESSION['userid'])) {
?>
			<!-- Topic comment form -->
			<button onclick="toggleFunction(); change()" id="change">&#9654; Add comment to topic</button>
			<div class="toggle">
				<form action="tcomments.php" method="POST" id="new_tcomment">
					<input type="hidden" name="topicid" value="<?=$topicid?>">
					<textarea type="text" name="tcomment" rows="5" cols="50" id="new_text" placeholder="Add your comment here..." required></textarea>
					<input type="submit" name="submit" value="Submit comment">
				</form>
			</div>
<?php
	}
?>

			<!-- List of comments -->
<?php
	if (isset($_GET['tpage'])) {
		$tpage = $_GET['tpage'];
	} else { 
		$tpage = 1; 
	}

	$results_per_page = 10;
	$start_from = ($tpage-1) * $results_per_page;

	$query = $db->prepare("SELECT tcomment.*, user.username, user.userid 
							FROM tcomment, user 
							WHERE tcomment.topicid=? AND user.userid=tcomment.userid
							ORDER BY tcommentid DESC
							LIMIT $start_from, $results_per_page");
	$query->execute(array($topicid));
	$tcomment = $query->fetchAll();
 
	foreach ($tcomment as $row) {
		$tcommentid = htmlspecialchars($row['tcommentid']);
		$tcomment = htmlspecialchars($row['comment']);
		$tuserid = htmlspecialchars($row['userid']);
		$username = htmlspecialchars($row['username']);
		$tcommentdate = htmlspecialchars($row['postdate']);
?>
			<div id="tcomment">
				<div class="t_comment"><?=$tcomment?></div>
				<div class="t_info">Submitted on <?=$tcommentdate?> by <a href="../Users/profile.php?userid=<?=$tuserid?>"><?=$username?></a>
<?php
		if (isset($myuser)) {
			if ($tuserid != $myuser) {
				$lquery = $db->prepare("SELECT * FROM tlikes WHERE userid=? AND tcommentid=?");
				$lquery->execute(array($myuser, $tcommentid));
				$results = $lquery->fetch();

				if (!empty($results)) {
?>
					<form action="../Likes/tunlike.php" method="POST" class="t_like">
						<input type="hidden" name="topicid" value="<?=$topicid?>">
<?php
					if (isset($_GET['tpage'])) {
						echo '<input type="hidden" name="tpage" value='.$_GET['tpage'].'>';
					} else {
						echo '<input type="hidden" name="tpage" value="1">';
					}
?>
						<input type="hidden" name="tcommentid" value="<?=$tcommentid?>">
						<input type="submit" value="Unlike" class="unlike">
					</form>
<?php
				} else {
?>
					<form action="../Likes/tlike.php" method="POST" class="t_like">
						<input type="hidden" name="topicid" value="<?=$topicid?>">
<?php
					if (isset($_GET['tpage'])) {
						echo '<input type="hidden" name="tpage" value='.$_GET['tpage'].'>';
					} else {
						echo '<input type="hidden" name="tpage" value="1">';
					}
?>
						<input type="hidden" name="tcommentid" value="<?=$tcommentid?>">
						<input type="submit" value="Like" class="like">
					</form>
<?php
				}
			} elseif ($tuserid == $myuser) {
?>
					<form id="tcommentform" action="delete_tcomment.php" method="POST" class="t_del">
	            		<input type="hidden" name="topicid" value="<?=$_GET['topicid']?>">
	            		<input type="hidden" name="tcommentid" value="<?=$tcommentid?>">
	            		<input type="submit" value="Delete">
	          		</form>
<?php
			}
		}
	echo "</div></div>";
	}
?>

			<!-- Page links -->		
<?php
	$query = $db->prepare("SELECT COUNT(*) AS total FROM tcomment WHERE topicid=?");
	$query->execute(array($topicid));
	$result = $query->fetchColumn();

	$total_pages = ceil($result / 10);

	if ($total_pages >= 1 && $tpage <= $total_pages) {
		echo "<a href='tcomments_form.php?topicid=".$topicid."&tpage=1' class='pages'";
		if ($tpage==1)
			echo " id='curPage'";
		echo ">1</a>";
		$i = max(2, $tpage - 3);
		if ($i > 2)
			echo " ... ";
		for (; $i < min($tpage + 4, $total_pages); $i++) {
			echo "<a href='tcomments_form.php?topicid=".$topicid."&tpage=".$i."' class='pages'";
			if ($tpage==$i)
				echo " id='curPage'";
			echo ">".$i."</a>";
		}
		if ($i != $total_pages)
			echo " ... ";
		echo "<a href='tcomments_form.php?topicid=".$topicid."&tpage=".$total_pages."' class='pages'";
		if ($tpage==$total_pages)
			echo " id='curPage'";
		echo ">".$total_pages."</a>";
	}
?>
		</section>
	</body>
</html>