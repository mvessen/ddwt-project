<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$topictitle = $_POST['topictitle'];
	$topicdes = $_POST['topicdes'];
	$userid = $_SESSION['userid'];

	$query = $db->prepare('INSERT INTO topic (title, description, userid, startdate)
							VALUES (?, ?, ?, CURRENT_TIMESTAMP)');
	$query->execute(array($topictitle, $topicdes, $userid));
	header('Location: ../Users/index.php');
?>