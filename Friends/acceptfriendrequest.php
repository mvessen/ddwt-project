<?php
    session_start();
    error_reporting(-1);
    ini_set("display_errors", 1);
    require_once('../config.inc.php');

    $db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

    $requestid = $_POST['requestid'];
    $user1 = $_POST['sender'];
    $user2 = $_POST['recipient'];

    $qh = $db->prepare('INSERT INTO friends (user1, user2) VALUES (?,?)');
    $qh->execute([$user1,$user2]);

    $query = $db->prepare('DELETE FROM friend_requests WHERE requestid = ?');
    $query->execute(array($requestid));

    header('Location: ../Users/profile.php?userid='.$user2.'');
?>
