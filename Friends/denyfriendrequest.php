<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
	
	$requestid = $_POST['requestid'];
	$userid = $_SESSION['userid'];
	
	$qh = $db->prepare("DELETE FROM friend_requests WHERE requestid = ?");
 	$qh->execute(array($requestid));

	header('location: ../Users/profile.php?userid='.$userid.'');
?>

