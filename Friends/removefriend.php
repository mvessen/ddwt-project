<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
	
	$relationid = $_POST['relationid'];
	$userid = $_SESSION['userid'];
	
	$qh = $db->prepare("DELETE FROM friends WHERE relationid = ?");
 	$qh->execute(array($relationid));

	header('location: ../Users/profile.php?userid='.$userid.'');
?>
