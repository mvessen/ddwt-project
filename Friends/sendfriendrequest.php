<?php
    session_start();
    error_reporting(-1);
    ini_set("display_errors", 1);
    require_once('../config.inc.php');

    $db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

    $sentID = $_POST['sentID'];
    $receiveID = $_POST['receiveID'];

    $qh = $db->prepare('INSERT INTO friend_requests (sender, recipient)
                     VALUES (?,?)');
    $qh->execute(array($sentID,$receiveID));

    header('Location: ../Users/profile.php?userid='.$receiveID.'');
?>
