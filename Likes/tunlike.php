<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$userid = $_SESSION['userid'];
	$tcommentid = $_POST['tcommentid'];
	$tpage = $_POST['tpage'];
	$topicid = $_POST['topicid'];

	$query = $db->prepare("DELETE FROM tlikes WHERE userid=? AND tcommentid=?");
	$query->execute(array($userid, $tcommentid));

	header('Location: ../Comments/tcomments_form.php?topicid='.$topicid.'&tpage='.$tpage.'')
?>