
# DDWT Project 'The Forum'

## Installatie instructies

### Step 1

Clone deze [repository](https://bitbucket.org/mvessen/ddwt-project/). De repository bevat alle directories en documenten die nodig zijn voor de webapplicatie. Verplaats de directory naar waar je het wil hebben.


### Step 2

Creëer de tabellen in de MySQL database. Alle tabellen (CREATE TABLE statements) zijn te vinden in het bestand databases.sql.


### Step 3

Kopieer config.mk.sample naar config.inc.php (zorg ervoor dat dit document zich in dezelfde directory bevind) en vul de gegevens van de database in.


### Step 4

Om ervoor te zorgen dat de juiste link in de verification e-mail komt te staan, verander de link in register.php op line 88. Zorg ervoor dat het deel van de link voor de 'Users' directory klopt met het pad waar de 'Users' directory zich bevind.


### Step 5

Open de webapplicatie.