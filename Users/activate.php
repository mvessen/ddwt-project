<?php session_start(); ?>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>Activated account</title>
	    <link rel="stylesheet" type="text/css" href="../main.css">
	    <script type="text/javascript" src="../toggle.js"></script>
  	</head>
	<body>
<?php
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	if (isset($_GET['email']) && !empty($_GET['email'])){	
		$email = explode(":",$_GET['email']);
		$emailhash = $email[1];
		$email = substr($email[0], 0, -9);

		$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

		$query = $db->prepare('SELECT * FROM user WHERE emailhash = ?'); 
		$query->execute(array($emailhash));
		$user = $query->fetchAll();
	
		if ($email == $user[0]['email'] && $emailhash == $user[0]['emailhash'] && $user[0]['activate'] == 0) {
			$query = $db->prepare('UPDATE user SET activate = ?'); 
			$query-> execute(array($user[0]['activate'] = '1'));
			$query = $db->prepare('SELECT * FROM user WHERE emailhash = ?');
			$query->execute(array($emailhash));
			$row = $query->fetch();		
			$_SESSION['userid'] = $row['userid'];
		}

		echo "<div id='act_container'>";
		echo "<a id='home' href='index.php'><img src='../forum.png' alt='forum logo'></a>";
		echo "<h2 id='act_succes'>Your account has been activated</h2>";
		echo "<p>You can now log in to your account</p>";
	}
?>
			<p>
				<a href="login_form.php">Login</a> or go to the
				<a href="index.php">Forum</a>.
			</p>
		</div>
	</body>
</html>