<?php session_start(); ?>
<!doctype html>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>Forum</title>
	    <link rel="stylesheet" type="text/css" href="../main.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	    <script type="text/javascript" src="../toggle.js"></script>
  	</head>
	<body>
		<header>
			<div id="header">
				<!-- Home -->
				<a id="home" href="index.php"><img src="../forum.png" alt="forum logo"></a>

<?php 
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');
	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	if (isset($_SESSION['userid'])) {
		$myuser = $_SESSION['userid'];			
?>
				<!-- Login/Register//Logout/Profile -->
				<div id="links">
					<a href="profile.php?userid=<?=$myuser?>">My profile</a>
					<a href="logout.php">Logout</a>
<?php
	} else {
?>
					<a href="login_form.php">Login</a>
					<a href="register_form.php">Register</a>
<?php  
	}
?>
				</div>
			</div>
		</header>

		<section>
			<h2>Welcome to the forum</h2>

			<!-- Live user search bar -->
			<form id="search-form" action="#">
				<div id="suggest">
					<input id="search" type="text" onkeydown="return (event.keyCode!=13);" onkeyup="suggest(this.value);" autocomplete="off" placeholder="Search a user...">
					<div id="suggestions" class="suggestionsBox">
						<div id="suggestionsList" class="suggestionList"> &nbsp; </div>
					</div>
				</div>
			</form>

			<div id="topic_container">
				<h3>Topics</h3>
				<!-- Live topic search bar -->
				<form id="topicsearch-form" action="#">
					<div id="topicsuggest">
						<input id="topicsearch" type="text" onkeydown="return (event.keyCode!=13);" onkeyup="topicsuggest(this.value);" autocomplete="off" placeholder="Search a topic...">
						<div id="topicsuggestions" class="suggestionsBox">
							<div id="topicsuggestionsList" class="suggestionList"> &nbsp; </div>
						</div>
					</div>
				</form>

				<!-- Topic form-->
<?php
	if (isset($_SESSION['userid'])) {
?>
				<button onclick="toggleFunction(); tchange()" id="tchange">&#9654; Start a new topic</button> <br>
				<div class="toggle">
					<form action="../Comments/topic.php" method="POST" id="new_topic">
						<input type="text" name="topictitle" id="new_title" placeholder="Topic title..." required>
						<textarea type="text" name="topicdes" maxlength="1000" rows="5" cols="50" id="new_desc" placeholder="Topic description..." required></textarea>
						<input type="submit" name="topic" value="Submit topic">
					</form>
				</div>
<?php
	}
?>

				<!-- List of topics -->
<?php
	if (isset($_GET['page'])) {
		$page = $_GET['page'];
	} else { 
		$page = 1; 
	}

	$results_per_page = 15;
	$start_from = ($page-1) * $results_per_page;

	$query = $db->prepare("SELECT topic.*, user.username 
						   FROM topic, user WHERE user.userid=topic.userid
						   ORDER BY topicid DESC
						   LIMIT $start_from, $results_per_page");
	$query->execute();
	$topic = $query->fetchAll();
 
	foreach ($topic as $row) {
		$topicid = htmlspecialchars($row['topicid']);
		$topictitle = htmlspecialchars($row['title']);
		$topicstarterid = htmlspecialchars($row['userid']);
		$topicstarter = htmlspecialchars($row['username']);
		$topicdate = htmlspecialchars($row['startdate']);
?>
				<div class="topics">
					<div class="t_title"><a href="../Comments/tcomments_form.php?topicid=<?=$topicid?>"><?=$topictitle?></a></div>
		          	<div class="t_info">Submitted on <?=$topicdate?> by <a href="profile.php?userid=<?=$topicstarterid?>"><?=$topicstarter?></a>
<?php
		if (isset($_SESSION['userid'])) {
			if ($topicstarterid == $_SESSION['userid']) {
?>
					<form action="../Comments/delete_topic.php" class="t_del" method="POST">
		            	<input type="hidden" name="topicid" value="<?=$topicid?>">
		            	<input type="submit" value="Delete topic">
		          	</form>
<?php
			}
		}
?>
					</div>
		        </div>
<?php
	}
?>

				<!-- Page links -->		
<?php
	$query = $db->prepare("SELECT COUNT(*) AS total FROM topic");
	$query->execute();
	$result = $query->fetchColumn();

	$total_pages = ceil($result / 15);

	if ($total_pages >= 1 && $page <= $total_pages) {
		echo "<a href='index.php?page=1' class='pages'";
		if ($page==1)
			echo " id='curPage'";
		echo ">1</a>";
		$i = max(2, $page - 3);
		if ($i > 2)
			echo " ... ";
		for (; $i < min($page + 4, $total_pages); $i++) {
			echo "<a href='index.php?page=".$i."' class='pages'";
			if ($page==$i)
				echo " id='curPage'";
			echo ">".$i."</a>";
		}
		if ($i != $total_pages)
			echo " ... ";
		echo "<a href='index.php?page=".$total_pages."' class='pages'";
		if ($page==$total_pages)
			echo " id='curPage'";
		echo ">".$total_pages."</a>";
	}
?>
			</div>
		</section>
	</body>
</html>