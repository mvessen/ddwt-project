<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php'); 
	require_once('../password.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$username = $_POST['username'];
	$password = $_POST['password'];

	if (isset($username) && isset($password)) {
		$query = $db->prepare('SELECT * FROM user WHERE username=?');
		$query->execute(array($username));
		$row = $query->fetch();

		$errors = "";

		if ($row && password_verify($password, $row['password_hash']) && $row['activate'] == 1) {
			$_SESSION['userid'] = $row['userid'];
			header('Location: .');
		} elseif (empty($row)) {
			$errors = 'Incorrect username or password. Please try again<br>';
		} elseif ($row['activate'] == 0) {
			$errors = 'Please activate your account before logging in<br>';
		} else {
			$errors = 'Incorrect username or password. Please try again<br>';
		}
		if (!empty($errors)) {
			$_SESSION['errors'] = $errors;
			header('Location: login_form.php');
			exit;
		}
	}
?>