<?php session_start(); ?>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>Login form</title>
	    <link rel="stylesheet" type="text/css" href="../main.css">
	    <script type="text/javascript" src="../toggle.js"></script>
  	</head>
	<body>
		<div id="login_form_container">
			<a id="home" href="index.php"><img src="../forum.png" alt="forum logo"></a>
			<h3> Login </h3>
			<div id="errors">
<?php 
	if(!empty($_SESSION['errors'])) {
		echo "<br><p>".$_SESSION['errors']."</p><br>";
	}
?>
			</div>
<?php unset($_SESSION['errors']);?>

			<form action="login.php" method="POST">
				<input type="text" name="username" pattern=".{5,100}" class="box" id="log_user" placeholder="Username">
				<input type="password" name="password"  pattern=".{5,20}" class="box" id= "log_pass" placeholder="Password">
				<input type="submit" name="Login" class="box" id="log_submit" value="Login">
			</form>
			<a href="index.php"><button class="box" id="log_cancel">Cancel</button></a>
		</div>
	</body>
</html>