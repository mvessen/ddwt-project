<?php
	session_start() ;
	session_destroy() ;
	error_reporting(-1);
	ini_set("display_errors", 1);
	header('Location: index.php');
?>