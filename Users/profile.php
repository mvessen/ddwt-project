<?php session_start(); ?>
<!doctype html>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>Profile</title>
	    <link rel="stylesheet" type="text/css" href="../main.css">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	    <script type="text/javascript" src="../toggle.js"></script>
  	</head>
	<body>
		<header>
			<div id="header">
				<!-- Home -->
				<a id="home" href="index.php"><img src="../forum.png" alt="forum logo"></a>

<?php 
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');
	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	if (isset($_SESSION['userid'])) {
		$myuser = $_SESSION['userid'];			
?>
				<!-- Login/Register//Logout/Profile -->
				<div id="links">
<?php
		if ($myuser != $_GET['userid']) {
?>
					<a href="profile.php?userid=<?=$myuser?>">My profile</a>
<?php
		}
?>
					<a href="logout.php">Logout</a>
<?php
	} else {
?>
					<a href="login_form.php">Login</a>
					<a href="register_form.php">Register</a>
<?php  
	}
?>
				</div>
			</div>
		</header>

<?php
	$profileid = htmlspecialchars($_GET['userid']);

	$query = $db->prepare("SELECT * FROM user WHERE userid=?");
	$query->execute(array($profileid));
	$user = $query->fetch();

	$username = htmlspecialchars($user['username']);
	$firstname = htmlspecialchars($user['firstname']);
	$lastname = htmlspecialchars($user['lastname']);
	$gender = htmlspecialchars($user['gender']);
	$study1 = htmlspecialchars($user['study1']);
	$study2 = htmlspecialchars($user['study2']);
	$studied = htmlspecialchars($user['studied']);
?>

		<section>
			<h1><?=$username?></h1>

			<!-- Friend Request -->
<?php  
	if (isset($_SESSION['userid'])) {
		$myuser = $_SESSION['userid'];

    	$query = $db->prepare("SELECT * FROM friend_requests WHERE sender=? AND recipient=?");
    	$query->execute(array($myuser, $profileid));  
    	$row = $query -> fetch();  
		$_query = $db->prepare("SELECT * FROM friends WHERE (user1=? AND user2=?) OR (user1=? AND user2=?)");
		$_query->execute(array($myuser, $profileid, $profileid, $myuser));
		$_row = $_query ->fetch();

    	if($_GET['userid'] != $_SESSION['userid'] && !$row && !$_row) {
?>
	        <form action="../Friends/sendfriendrequest.php" method="POST" id="friend_request">
	            <input type="hidden" name="sentID" value="<?=$myuser?>">
	            <input type="hidden" name="receiveID" value="<?=$profileid?>">
	            <input type="submit" value="Send friend request">
	        </form>
<?php
		}
	}
?>
		
			<!-- Live user search bar -->
			<form id="search-form" action="#">
				<div id="suggest">
					<input id="search" type="text" onkeyup="suggest(this.value);" autocomplete="off" placeholder="Search a user...">
					<div id="suggestions" class="suggestionsBox">
						<div id="suggestionsList" class="suggestionList"> &nbsp; </div>
					</div>
				</div>
			</form>

			<!-- Tab links -->
			<div class="tab">
				<button class="tablinks" onclick="openTab(event,'personal')" id="defaultOpen">Personal</button>
				<button class="tablinks" onclick="openTab(event,'comments')">Comments</button>
				<button class="tablinks" onclick="openTab(event,'topicsS')">Topics</button>
				<button class="tablinks" onclick="openTab(event,'likes')">Likes</button>
				<button class="tablinks" onclick="openTab(event,'friends')">Friends</button>
			</div>

			<!-- Open personal tab when page loads -->	
			<script>
				window.onload = function() {
					document.getElementById("defaultOpen").click();
				}
			</script>

			<!-- Tab content -->

			<!-- Personal -->
			<div id="personal" class="tabcontent">
				<div id="pers_info">
					<div id="pers_name">
						<h3>Personal information</h3>
						<?=$firstname." ".$lastname?> 
<?php
		if ($gender == "Female") {
			echo "<div class='fa fa-venus' id='female'></div>";
		} else if ($gender == "Male") {
			echo "<div class='fa fa-mars' id='male'></div>";
		}
?>
					</div>
					<div id="pers_studies">
						<b>Current first studie</b><br><?=$study1?><br><br>
						<b>Current second studie</b><br><?=$study2?><br><br>
						<b>Completed study</b><br><?=$studied?>
					</div>
<?php
	if (isset($_SESSION['userid'])) {
		if ($_GET['userid'] == $_SESSION['userid']) {
?>
					<!-- Edit profile link -->
					<a id="edit" href="update_form.php">Edit profile</a>
<?php
		}
	}
?>
				</div>
			</div>

			<!-- Comments --> 
			<div id="comments" class="tabcontent">
				<h3>Comments</h3>
<?php
	if (isset($_SESSION['userid'])) {			
?>
				<form action="../Comments/comments.php" method="POST">
					<input type="hidden" name="profileuser" value="<?=$profileid?>">
					<textarea type="text" name="comment" rows="5" cols="50" id="prof_new_text" placeholder="Add your comment here..."></textarea>
					<input type="submit" name="submit" id="prof_submit_text" value="Submit comment">
				</form>
<?php
	}

	$query = $db->prepare('SELECT sender.username AS sendername, sender.userid AS senderid, 
							receiver.username AS receivername, receiver.userid AS receiverid,
							comment.comment, comment.senddate, comment.commentid
							FROM comment 
							LEFT JOIN user AS sender ON comment.sender=sender.userid
							LEFT JOIN user AS receiver ON comment.receiver=receiver.userid
							WHERE comment.sender=? OR comment.receiver=?
							ORDER BY comment.commentid DESC');
	$query->execute(array($profileid, $profileid));
	$result = $query->fetchAll();

	if (empty($result)) {
		echo "No comments found";
	}

	foreach($result as $row) {
		$commentid = htmlspecialchars($row['commentid']);
		$comment = htmlspecialchars($row['comment']);
		$sender = htmlspecialchars($row['sendername']);
		$senderid = htmlspecialchars($row['senderid']);
		$receiver = htmlspecialchars($row['receivername']);
		$receiverid = htmlspecialchars($row['receiverid']);
		$senddate = htmlspecialchars($row['senddate']);
?>
				<div class="profilecomments">
					<div class="t_info com">
						<a href="profile.php?userid=<?=$senderid?>"><?=$sender?></a> &#8594; 
						<a href="profile.php?userid=<?=$receiverid?>"><?=$receiver?></a></td>
						<?=$senddate?>
<?php
	if(isset($_SESSION['userid'])) {
		if ($_GET['userid'] == $_SESSION['userid']) {
?>
						<form action="../Comments/delete_comment.php" method="POST" class="t_del">
							<input type="hidden" name="commentid" value="<?=$commentid?>">
	            			<input type="submit" value="Delete">
						</form>
<?php
			}
		}
?>
					</div>
					<div class="prof_message"><?=$comment?></div>
				</div>
<?php
	}
?>
			</div>

			<!-- Topics -->
			<div id="topicsS" class="tabcontent">
				<h3>Topics Started</h3>
<?php
	$query = $db->prepare('SELECT * FROM topic WHERE userid=? ORDER BY topicid DESC');
	$query->execute(array($_GET['userid']));
	$topics = $query->fetchAll();

	if (empty($topics)) {
		echo "No topics found";
	}

	foreach ($topics as $topic) {
		$topicid = htmlspecialchars($topic['topicid']);
		$title = htmlspecialchars($topic['title']);
		$date = htmlspecialchars($topic['startdate']);
?>
				<div class="topics">
					<div class="t_title"><a href="../Comments/tcomments_form.php?topicid=<?=$topicid?>"><?=$title?></a></div>
					<div class="t_info">Submitted on <?=$date?></div>
				</div>
<?php
	}
?>
			</div>

			<!-- Likes -->
			<div id="likes" class="tabcontent">
				<h3>Likes</h3>
<?php
	$query = $db->prepare("SELECT tcomment.comment, user.username, topic.title, tcomment.postdate
							FROM tlikes, tcomment, user, topic
							WHERE tlikes.userid = ?
							AND tcomment.tcommentid = tlikes.tcommentid
							AND topic.topicid = tcomment.topicid
							AND user.userid = tcomment.userid");

	$query->execute(array($_GET['userid']));
	$likes = $query->fetchAll();

	if (empty($likes)) {
		echo "No likes found";
	}

	foreach ($likes as $like) {
		$comment = htmlspecialchars($like['comment']);
		$commenter = htmlspecialchars($like['username']);
		$title = htmlspecialchars($like['title']);
		$date = htmlspecialchars($like['postdate']);
?>
				<div class="likes">
					<div class="t_title"><a href="../Comments/tcomments_form.php?topicid=<?=$topicid?>"><?=$title?></a></div>
					<div class="l_comment"><?=$comment?></div>
					<div class="t_info">Posted on <?=$date?> by <?=$commenter?></div>
				</div>
<?php
	}
?>
			</div>

			<!-- Friends -->
			<div id="friends" class="tabcontent">
				<h3>Friends</h3>
				<div id="friends_info">

<?php
	if (isset($_SESSION['userid']))	{
		if($_GET['userid'] == $_SESSION['userid']) {	
			$query = $db->prepare("SELECT requestid, sender, recipient FROM friend_requests WHERE recipient=?");
	        $query->execute(array($_SESSION['userid']));

	        $count = $query->rowCount();

	        if ($count>0) {
?>
					<div class="request_box">
						<p>You've got <?=$count?> friend request(s):</p>
<?php		
				foreach($query as $row) {
		            $requestid = htmlspecialchars($row['requestid']);
					$sender = htmlspecialchars($row['sender']);
		            $recipient = htmlspecialchars($row['recipient']);
		            $_query = $db->prepare("SELECT userid, username FROM user WHERE userid = '$sender'");
		            $_query->execute();

					foreach($_query as $row) {
		 				$username = htmlspecialchars($row['username']);
?>
							<!-- Friend requests -->
							<div class="friend_req">
								<div class="req_name"><a href="profile.php?userid=<?=$sender?>"><?=$username?></a></div>
							    <form action="../Friends/acceptfriendrequest.php" method="POST" class="accept">
							        <input type="hidden" name="requestid" value="<?=$requestid?>">
							        <input type="hidden" name="sender" value="<?=$sender?>">
							        <input type="hidden" name="recipient" value="<?=$recipient?>">
							       	<input type="submit" value="Accept">
								</form>
								<form action="../Friends/denyfriendrequest.php" method="POST" class="deny">
							      	<input type="hidden" name="requestid" value="<?=$requestid?>">
							       	<input type="submit" value="Deny">
							    </form>
							</div>
<?php
					}
				}
?>
					</div>
<?php
			}
		}
	}

	$query = $db->prepare("SELECT relationid, user1, user2 FROM friends WHERE user1=? OR user2=?");
	$query-> execute(array($_GET['userid'], $_GET['userid']));
	$friends = $query->fetchAll();

	if (empty($friends)) {
		echo "No friends found";
	}

	foreach($friends as $row) {
		$relationid = htmlspecialchars($row['relationid']);
		$user1 = htmlspecialchars($row['user1']);
		$user2 = htmlspecialchars($row['user2']);

		$_query = $db->prepare("SELECT userid, username FROM user WHERE userid!=? AND (userid=? OR userid=?)");
	 	$_query->execute(array($_GET['userid'], $user1, $user2));

		foreach($_query as $row) {
			$username = htmlspecialchars($row['username']);
			$userid = htmlspecialchars($row['userid']);
?>			
					<!-- Friends list -->
					<div class="friendslist">
						<div class="friend_user"><a href="profile.php?userid=<?=$userid?>"><?=$username?></a></div>
<?php
			if (isset($_SESSION['userid'])) {
				if($_GET['userid'] == $_SESSION['userid']) {
?>
						<form action="../Friends/removefriend.php" method="POST" class="unfriend_user">
							<input type="hidden" name="relationid" value="<?=$relationid?>">
							<input type="submit" value="Unfriend">
						</form>
<?php
				}
			}
		}
	}
?>
					</div>
				</div>
			</div>

		</section>
	</body>
</html>
	
