<?php session_start(); ?>
<!doctype html>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>Registration succesful</title>
	    <link rel="stylesheet" type="text/css" href="../main.css">
  	</head>
	<body>
<?php
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');
	require_once('../password.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$username = $_POST['username'];
	$password = $_POST['password'];
	$email = $_POST['email'] . "@student.rug.nl";
	$email1 = explode("@",$email);

    $query = $db->prepare('SELECT * FROM user WHERE username = ?'); 
	$query->execute(array($username));
	$user = $query->fetchAll();


	$mistakes = false;
	$errors = array();

	if (!empty($user)) {
		$errors[] = "This username is already taken, choose another one<br>"; 
		$mistakes = true;
	}

	if ((!empty($username) && (strlen($username)<5 || strlen($username)>100))) {
		$errors[] = "Your username should be between 5 and 100 characters<br>";
		$mistakes = true;
	}

	if ((!empty($password) && (strlen($password)<5 || strlen($password)>20))) {
		$errors[] = "Your password should be between 5 and 20 characters<br>";
		$mistakes = true;
	}

	if (!preg_match('/^[A-Za-z0-9]*$/', $username)) {
		$errors[] = "Username can only contain letters and numbers<br>";
		$mistakes = true;
	}

	if (!empty($email1[0]) && (strlen($email1[0])<4 || strlen($email1[0])>85)) {
		$errors[] = "Your student mail should be between 4 and 100 characters<br>";
		$mistakes = true;
	}

	if (!empty($errors)) {
		$_SESSION['errors'] = $errors;
		header('Location: register_form.php');
		exit;
	}

	if(!$mistakes) {
		$activate = 0;
		$emailhash = md5(rand(0,1000));
	 	$passwordhash = password_hash($password, PASSWORD_DEFAULT);
	 	$query = $db->prepare('INSERT INTO user (username, password_hash, email, emailhash, activate) VALUES (?, ?, ?, ?, ?)');
		$query->execute(array($username, $passwordhash, $email, $emailhash, $activate));

		echo "<div id='reg_container'>";
		echo "<a id='home' href='index.php'><img src='../forum.png' alt='forum logo'></a>";
		echo "<h2 id='reg_succes'>Registration succesful.</h2>"; 
		echo "<p>A verification email has been sent.<br>It may take a few minutes to arrive.</p>";
		echo "<h4 id='no_mail'>If you have not received a verification email</h4>";
		echo "<p>Check your junk mail box to see if the verification email is there</p>";
		echo "<p id='forum_link'>Go back to the <a href='index.php'>Forum</a></p>";
		echo "</div>";

		$to      = $email; 
		$subject = 'Signup | Activation'; 
		$message = '
		 
		Thank you for registering at the forum!

		IMPORTANT:
		To activate your account, either click the link below, or copy the address in your browser.
		http://localhost:8080/project/Users/activate.php?email='.$email.'emailhash:'.$emailhash.'
		 
		'; /* Change part of link before 'Users' to your website's path to the 'Users' directory */ 
		mail($to, $subject, $message);
	}
?>
	</body>
</html>