<?php session_start(); ?>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>Register form</title>
	    <link rel="stylesheet" type="text/css" href="../main.css">
	    <script type="text/javascript" src="../toggle.js"></script>
  	</head>
	<body>
		<div id="reg_form_container">
			<a id="home" href="index.php"><img src="../forum.png" alt="forum logo"></a>
			<h3> Register </h3>
			<div id="errors">
<?php 
	if(!empty($_SESSION['errors'])) {
?>
				<ul>
<?php
		foreach ($_SESSION['errors'] as $error) {
?>
					<li><?=$error?></li>
<?php
		}
?>
				</ul>
<?php
	}
?>
			</div>
<?php unset($_SESSION['errors']);?>

			<form action="register.php" method="POST">
				<input type="text" name="username" placeholder="Username" class="box" id="reg_user" required>
				<input type="password" name="password" placeholder="Password" class="box" id="reg_pass" required>
				<p class="box"><input type="text" name="email" placeholder="j.baker" id="reg_mail" required>@student.rug.nl</p>
				<input type="submit" name="Register" id="reg_submit" value="Register">
			</form>
			<a href="index.php"><button id="reg_cancel">Cancel</button></a>
		</div>
	</body>
</html>