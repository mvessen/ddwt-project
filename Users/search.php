<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$name = $_POST['queryString'];

	if (!preg_match('/^[a-z0-9\040]+$/i', $name)) {
		die("Use only letters and numbers");
	} else {
		$query = $db->prepare('SELECT username, userid FROM user WHERE username LIKE ? LIMIT 10');
		$query->execute(array("%$name%"));
		$data = $query->fetchAll();
		$num_rows = count($data);

		if ($num_rows>0) {
			foreach($data as $row) {
				$username = htmlspecialchars($row['username']);
				$userid = htmlspecialchars($row['userid']);

				echo "<div class='sugg'><a href='profile.php?userid=".$userid."'>".$username."</a></div>";
			} 
		} else {
			echo "<div class='sugg'>No user found</div>";
		}
	}
?>