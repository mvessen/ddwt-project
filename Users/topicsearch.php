<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$topic = $_POST['queryString'];

	$query = $db->prepare('SELECT title, topicid FROM topic WHERE title LIKE ? LIMIT 10');
	$query->execute(array("%$topic%"));
	$data = $query->fetchAll();
	$num_rows = count($data);

	if ($num_rows>0) {
		foreach($data as $row) {
			$title = htmlspecialchars($row['title']);
			$topicid = htmlspecialchars($row['topicid']);

			echo "<div class='sugg'><a href='../Comments/tcomments_form.php?topicid=".$topicid."'>".$title."</a></div>";
		} 
	} else {
			echo "<div class='sugg'>No topic found</div>";
	}
?>