<?php
	session_start();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../config.inc.php');
	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$mistakes = false;

	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$gender = $_POST['gender'];
	$study1 = $_POST['study1'];
	$study2 = $_POST['study2'];
	$studied = $_POST['studied'];

	if ((!empty($firstname) && !preg_match('/^[a-zA-Z][a-z]*( [a-zA-Z][a-z]*)*$/', $firstname)) || (!empty($lastname) && !preg_match('/^[a-zA-Z][a-z]*( [a-zA-Z][a-z]*)*$/', $lastname))) {
		echo "Your first- and lastname can only contain letters. Multiple names should be seperated by a space<br>";
		$mistakes = true;
	}

	if ((!empty($firstname) && strlen($firstname)>100) || (!empty($lastname) && strlen($lastname)>100)) {
		echo "Your first- and lastname should not be longer than 100 characters<br>";
		$mistakes = true;		
	}

	if (isset($_POST['study1']) && isset($study2) && substr($study1, 0, -7)==substr($study2, 0, -7) && $study1!="none" && $study2!="none") {
		echo "Your first and second study cannot be the same<br>"; 
		$mistakes = true;
		}

	if (isset($study1) && isset($study2) && isset($studied) && (substr($study1, 0, -7)==$studied || substr($study2, 0, -7)==$studied )) {
		echo "Your completed study cannot be the same as your first or second study<br>";
		$mistakes = true;
	}
	if (!empty($study1) && !empty($study2) && $study1=="none" && $study2!="none") {
		echo "First fill in your first study<br>";
		$mistakes = true;
	}
		
 	if(!$mistakes) {
		$query = $db->prepare('UPDATE user SET firstname = ? , lastname = ? , gender = ?, study1 = ? , study2 = ? , studied = ? WHERE userid = ?'); 
		$query-> execute(array($firstname, $lastname, $gender, $study1, $study2, $studied, $_SESSION['userid']));
		header('Location: profile.php?userid='.$_SESSION['userid']); 	
	}
?>