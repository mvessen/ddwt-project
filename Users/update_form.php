<?php session_start(); ?>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>Edit profile</title>
	    <link rel="stylesheet" type="text/css" href="../main.css">
	    <script type="text/javascript" src="../toggle.js"></script>
  	</head>
	<body>
<?php
	require_once('../config.inc.php');

	error_reporting(-1);
	ini_set("display_errors", 1);

	$db = new PDO("mysql:dbname=".$config['db_name'].";host=".$config['db_host'],
              $config['db_user'], $config['db_pass'],
              [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

	$query = $db->prepare('SELECT * FROM user WHERE userid= ?');
	$query->execute(array($_SESSION['userid']));
	$users = $query->fetchAll();

	foreach ($users as $user) {
		$firstname = htmlspecialchars($user['firstname']);
		$lastname = htmlspecialchars($user['lastname']);
		$gender = htmlspecialchars($user['gender']);
		$study1 = htmlspecialchars($user['study1']);
		$study2 = htmlspecialchars($user['study2']);
		$studied = htmlspecialchars($user['studied']);
?>

		<h3>
			Update your profile
		</h3>
		<form action="update.php" method="POST">
			<input type="text" name="firstname" placeholder="John" value="<?= $firstname; ?>"> 
			<input type="text" name="lastname" placeholder="Baker" value="<?= $lastname; ?>"><br>
			<input type="radio" name="gender" <?=$gender=="Male"? "checked" : ""?> value="Male">Male<br> 
			<input type="radio" name="gender" <?=$gender=="Female"? "checked" : ""?> value="Female">Female<br>
			<p>First Study</p>
			<select name="study1">
<?php 
		$studies1 = array( 'none',
				 'Ba American Studies year 1',
				 'Ba American Studies year 2',
				 'Ba American Studies year 3',

				 'Ba Archaeology year 1',
				 'Ba Archaeology year 2',
				 'Ba Archaeology year 3',

				 'Ba Art History year 1',
				 'Ba Art History year 2',
				 'Ba Art History year 3',

				 'Ba Arts, Culture and Media year 1',
				 'Ba Arts, Culture and Media year 2',
				 'Ba Arts, Culture and Media year 3',

				 'Ba Classical Studies year 1',
				 'Ba Classical Studies year 2',
				 'Ba Classical Studies year 3',

				 'Ba Communication and Information Studies year 1',
				 'Ba Communication and Information Studies year 2',
				 'Ba Communication and Information Studies year 3',

				 'Ba Dutch Language and Culture year 1',
				 'Ba Dutch Language and Culture year 2',
				 'Ba Dutch Language and Culture year 3',

				 'Ba Dutch Studies year 1',
				 'Ba Dutch Studies year 2',

				 'Ba English Language and Culture year 1',
				 'Ba English Language and Culture year 2',
				 'Ba English Language and Culture year 3',

				 'Ba European Languages and Cultures  year 1',
				 'Ba European Languages and Cultures  year 2',
				 'Ba European Languages and Cultures  year 3',

				 'Ba History year 1',
				 'Ba History year 2',
				 'Ba History year 3',

				 'Ba Information Science year 1',
				 'Ba Information Science year 2',
				 'Ba Information Science year 3',

				 'Ba International Relations and International Organization year 1',
				 'Ba International Relations and International Organization year 2',
				 'Ba International Relations and International Organization year 3',

				 'Ba Linguistics year 1',
				 'Ba Linguistics year 2',
				 'Ba Linguistics year 3',

				 'Ba Media Studies year 1',
				 'Ba Media Studies year 2',
				 'Ba Media Studies year 3',

				 'Ba Middle Eastern Studies year 1',
				 'Ba Middle Eastern Studies year 2',
				 'Ba Middle Eastern Studies year 3',

				 'Ba Minorities and Multilingualism year 1',
				 'Ba Minorities and Multilingualism year 2',
				 'Ba Minorities and Multilingualism year 3',

				 'Ma Archaeology year 1',
				 'Ma Arts and Culture year 1',
				 'Ma Classics and Ancient Civilizations year 1',
				 'Ma Communication and Information Studies year 1',
				 'Ma Dutch Studies year 1',

				 'Ma European Studies: Euroculture  year 1',
				 'Ma European Studies: Euroculture  year 2',

				 'Ma History year 1',
				 'Ma Interdisciplinairy Seminars year 1',
				 'Ma International Relations year 1',
				 'Ma International Relations: International Humanitarian Action year 1',
				 'Ma Linguistics year 1',
				 'Ma Literary Studies year 1',
				 'Ma Mediastudies year 1',
				 'Ma Middle Eastern Studies year 1',
				 'Ma North American Studies year 1',

				 'MaEduc Dutch Language and Culture year 1',
				 'MaEduc Dutch Language and Culture year 2',

				 'MaEduc English Language and Culture year 1',
				 'MaEduc English Language and Culture year 2',

				 'MaEduc French Language and Culture year 1',
				 'MaEduc French Language and Culture year 2',

				 'MaEduc Frisian Language and Literature year 1',
				 'MaEduc Frisian Language and Literature year 2',

				 'MaEduc German Language en Culture year 1',
				 'MaEduc German Language en Culture year 2',

				 'MaEduc Greek and Latin year 1',
				 'MaEduc Greek and Latin year 2',

				 'MaEduc History year 1',
				 'MaEduc History year 2',

				 'MaEduc Spanish Language and Culture year 1',
				 'MaEduc Spanish Language and Culture year 2',

				 'ReMa Archaeology year 1',
				 'ReMa Archaeology year 2',

				 'ReMa Arts and Culture year 1',
				 'ReMa Arts and Culture year 2',

				 'ReMa Classics and Ancient Civilizations year 1',
				 'ReMa Classics and Ancient Civilizations year 2',

				 'ReMa History year 1',
				 'ReMa History year 2',

				 'ReMa International Relations year 1',
				 'ReMa International Relations year 2',

				 'ReMa Linguistics year 1',
				 'ReMa Linguistics year 2',

				 'ReMa Literary Studies year 1',
				 'ReMa Literary Studies year 2', );
		
		foreach ($studies1 as $studyone) {
			if ($study1 == $studyone) {
				echo "<option selected='selected'>".$studyone."</option>";
			}else{
				echo "<option>".$studyone."</option>";
			}
		}
?>
			</select>
			<br>
			<p>Second study</p>
			<select name="study2">
<?php
		$studies2 = array(
				 'none',
				 'Ba American Studies year 1',
				 'Ba American Studies year 2',
				 'Ba American Studies year 3',

				 'Ba Archaeology year 1',
				 'Ba Archaeology year 2',
				 'Ba Archaeology year 3',

				 'Ba Art History year 1',
				 'Ba Art History year 2',
				 'Ba Art History year 3',

				 'Ba Arts, Culture and Media year 1',
				 'Ba Arts, Culture and Media year 2',
				 'Ba Arts, Culture and Media year 3',

				 'Ba Classical Studies year 1',
				 'Ba Classical Studies year 2',
				 'Ba Classical Studies year 3',

				 'Ba Communication and Information Studies year 1',
				 'Ba Communication and Information Studies year 2',
				 'Ba Communication and Information Studies year 3',

				 'Ba Dutch Language and Culture year 1',
				 'Ba Dutch Language and Culture year 2',
				 'Ba Dutch Language and Culture year 3',

				 'Ba Dutch Studies year 1',
				 'Ba Dutch Studies year 2',

				 'Ba English Language and Culture year 1',
				 'Ba English Language and Culture year 2',
				 'Ba English Language and Culture year 3',

				 'Ba European Languages and Cultures  year 1',
				 'Ba European Languages and Cultures  year 2',
				 'Ba European Languages and Cultures  year 3',

				 'Ba History year 1',
				 'Ba History year 2',
				 'Ba History year 3',

				 'Ba Information Science year 1',
				 'Ba Information Science year 2',
				 'Ba Information Science year 3',

				 'Ba International Relations and International Organization year 1',
				 'Ba International Relations and International Organization year 2',
				 'Ba International Relations and International Organization year 3',

				 'Ba Linguistics year 1',
				 'Ba Linguistics year 2',
				 'Ba Linguistics year 3',

				 'Ba Media Studies year 1',
				 'Ba Media Studies year 2',
				 'Ba Media Studies year 3',

				 'Ba Middle Eastern Studies year 1',
				 'Ba Middle Eastern Studies year 2',
				 'Ba Middle Eastern Studies year 3',

				 'Ba Minorities and Multilingualism year 1',
				 'Ba Minorities and Multilingualism year 2',
				 'Ba Minorities and Multilingualism year 3',

				 'Ma Archaeology year 1',
				 'Ma Arts and Culture year 1',
				 'Ma Classics and Ancient Civilizations year 1',
				 'Ma Communication and Information Studies year 1',
				 'Ma Dutch Studies year 1',

				 'Ma European Studies: Euroculture  year 1',
				 'Ma European Studies: Euroculture  year 2',

				 'Ma History year 1',
				 'Ma Interdisciplinairy Seminars year 1',
				 'Ma International Relations year 1',
				 'Ma International Relations: International Humanitarian Action year 1',
				 'Ma Linguistics year 1',
				 'Ma Literary Studies year 1',
				 'Ma Mediastudies year 1',
				 'Ma Middle Eastern Studies year 1',
				 'Ma North American Studies year 1',

				 'MaEduc Dutch Language and Culture year 1',
				 'MaEduc Dutch Language and Culture year 2',

				 'MaEduc English Language and Culture year 1',
				 'MaEduc English Language and Culture year 2',

				 'MaEduc French Language and Culture year 1',
				 'MaEduc French Language and Culture year 2',

				 'MaEduc Frisian Language and Literature year 1',
				 'MaEduc Frisian Language and Literature year 2',

				 'MaEduc German Language en Culture year 1',
				 'MaEduc German Language en Culture year 2',

				 'MaEduc Greek and Latin year 1',
				 'MaEduc Greek and Latin year 2',

				 'MaEduc History year 1',
				 'MaEduc History year 2',

				 'MaEduc Spanish Language and Culture year 1',
				 'MaEduc Spanish Language and Culture year 2',

				 'ReMa Archaeology year 1',
				 'ReMa Archaeology year 2',

				 'ReMa Arts and Culture year 1',
				 'ReMa Arts and Culture year 2',

				 'ReMa Classics and Ancient Civilizations year 1',
				 'ReMa Classics and Ancient Civilizations year 2',

				 'ReMa History year 1',
				 'ReMa History year 2',

				 'ReMa International Relations year 1',
				 'ReMa International Relations year 2',

				 'ReMa Linguistics year 1',
				 'ReMa Linguistics year 2',

				 'ReMa Literary Studies year 1',
				 'ReMa Literary Studies year 2', );

		foreach ($studies2 as $studytwo) {
			if ($study2 == $studytwo) {
				echo "<option selected='selected'>".$studytwo."</option>";
			}else{
				echo "<option>".$studytwo."</option>";
			}
		}
?>
			</select>
			<br>
			<p>Completed study</p>
			<select name="studied">
<?php
		$studies = array(
				 'none',
				 'Ba American Studies',
				 'Ba Archaeology',
				 'Ba Art History',
				 'Ba Arts, Culture and Media',
				 'Ba Classical Studies',
				 'Ba Communication and Information Studies',
				 'Ba Dutch Language and Culture',
				 'Ba Dutch Studies',
				 'Ba English Language and Culture',
				 'Ba European Languages and Cultures ',
				 'Ba History',
				 'Ba Information Science',
				 'Ba International Relations and International Organization',
				 'Ba Linguistics',
				 'Ba Media Studies',
				 'Ba Middle Eastern Studies',
				 'Ba Minorities and Multilingualism',
				 'Ma Archaeology',
				 'Ma Arts and Culture',
				 'Ma Classics and Ancient Civilizations',
				 'Ma Communication and Information Studies',
				 'Ma Dutch Studies',
				 'Ma European Studies: Euroculture ',
				 'Ma History',
				 'Ma Interdisciplinairy Seminars',
				 'Ma International Relations',
				 'Ma International Relations: International Humanitarian Action',
				 'Ma Linguistics',
				 'Ma Literary Studies',
				 'Ma Mediastudies',
				 'Ma Middle Eastern Studies',
				 'Ma North American Studies',
				 'MaEduc Dutch Language and Culture',
				 'MaEduc English Language and Culture',
				 'MaEduc French Language and Culture',
				 'MaEduc Frisian Language and Literature',
				 'MaEduc German Language en Culture',
				 'MaEduc Greek and Latin',
				 'MaEduc History',
				 'MaEduc Spanish Language and Culture',
				 'ReMa Archaeology',
				 'ReMa Arts and Culture',
				 'ReMa Classics and Ancient Civilizations',
				 'ReMa History',
				 'ReMa International Relations',
				 'ReMa Linguistics',
				 'ReMa Literary Studies', );

		foreach ($studies as $study) {
			if ($studied == $study) {
				echo "<option selected='selected'>".$study."</option>";
			}else{
				echo "<option>".$study."</option>";
			}
		}
?>
			</select>
			<br>
			<input type="submit" value="Submit">
		</form>
<?php
	}
?>
		<a href="profile.php?userid=<?=$_SESSION['userid']?>"><button>Cancel</button></a>
	</body>
</html>