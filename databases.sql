CREATE TABLE user (
	userid INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	username VARCHAR(100) NOT NULL, 
	password_hash CHAR(60) NOT NULL,
	firstname VARCHAR(100), 
	lastname VARCHAR(100), 
	gender VARCHAR(6),
	study1 VARCHAR(100),
	study2 VARCHAR(100),
	studied VARCHAR(100),
	email VARCHAR(100),
	emailhash  VARCHAR(32),
	activate INT(1)
);

CREATE TABLE topic (
	topicid INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	title VARCHAR(100) NOT NULL,
	description TEXT NOT NULL,
	userid INT NOT NULL,
	startdate DATETIME NOT NULL
);

CREATE TABLE tcomment (
	tcommentid INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	comment TEXT NOT NULL,
	userid INT NOT NULL,
	topicid INT NOT NULL,
	postdate DATETIME NOT NULL
);

CREATE TABLE comment (
	commentid INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	comment TEXT NOT NULL,
	sender INT NOT NULL,
	receiver INT NOT NULL,
	senddate DATETIME NOT NULL
);

CREATE TABLE friend_requests (
	requestid INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	sender INT NOT NULL,
	recipient INT NOT NULL
);

CREATE TABLE friends (
	relationid INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	user1 INT NOT NULL,
	user2 INT NOT NULL
);

CREATE TABLE tlikes (
	tlikeid INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	userid INT NOT NULL,
	tcommentid INT NOT NULL
);