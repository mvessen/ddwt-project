// Toggle functions
function toggleFunction() {
    var x = document.getElementsByClassName("toggle")[0];
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}

function change() {
	var c = document.getElementById("change");
	if (c.innerHTML == "▶ Add comment to topic") {
		c.innerHTML = "▼ Add comment to topic";
	} else {
		c.innerHTML = "▶ Add comment to topic";
	}
}

function tchange() {
	var c = document.getElementById("tchange");
	if (c.innerHTML == "▶ Start a new topic") {
		c.innerHTML = "▼ Start a new topic";
	} else {
		c.innerHTML = "▶ Start a new topic";
	}
}

// Search bar functions
function suggest(inputString){
	if(inputString.length == 0) {
		$('#suggestions').fadeOut("fast");
	} else {
		$.post("search.php", {queryString: ""+inputString+""}, function(data){
			if(data.length > 0) {
				$('#suggestions').fadeIn("fast");
				$('#suggestionsList').html(data);
			}
		});
	}
}

function topicsuggest(inputString){
	if(inputString.length == 0) {
		$('#topicsuggestions').fadeOut("fast");
	} else {
		$.post("topicsearch.php", {queryString: ""+inputString+""}, function(data){
			if(data.length > 0) {
				$('#topicsuggestions').fadeIn("fast");
				$('#topicsuggestionsList').html(data);
			}
		});
	}
}

// Tab function
function openTab(evt, tabName) {
	var i, tabcontent, tablinks;

	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}

	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace("active", "");
	}

	document.getElementById(tabName).style.display = "block";
	evt.currentTarget.className += " active";
}

// Show error function 
function showHide() {
	if($.trim($("#errors").text()) == '') {
    $("#errors").hide();
	}
}